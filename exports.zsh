# Prefer US English and use UTF-8
export LANG="en_US.UTF-8";
export LC_ALL="en_US.UTF-8";
export SHELL=/Users/dan/brew/bin/zsh

# Set nano as preferred editor
export EDITOR="nano";

# GPG
export GPG_TTY=$(tty);

# NVM settings
export NVM_DIR="$HOME/.nvm"
export NVM_LAZY_LOAD=true

# PATH setup

# Local bin
export PATH="$HOME/bin:$PATH"
# go bin
export PATH="$HOME/go/bin:$PATH"
# Brew bins
export PATH="$HOME/brew/bin:$PATH"
export PATH="$HOME/brew/sbin:$PATH"
# php
export PATH="$HOME/.composer/vendor/bin:$PATH"
# python
export PATH="$HOME/brew/opt/python/libexec/bin:$PATH"
export PATH="$(yarn global bin):$PATH"
export PATH="$PATH:/Users/dan/.dotnet/tools"

if which ruby >/dev/null && which gem >/dev/null; then
  PATH="$(ruby -r rubygems -e 'puts Gem.user_dir')/bin:$PATH"
fi

export PATH="/Users/dan/Library/Python/3.9/bin:$PATH"

#export PATH="/Users/dan/brew/opt/php@8.0/bin:$PATH"
#export PATH="/Users/dan/brew/opt/php@8.0/sbin:$PATH"
#export PATH="/Users/dan/brew/opt/php@7.4/bin:$PATH"
#export PATH="/Users/dan/brew/opt/php@7.4/sbin:$PATH"
#export PATH="/Users/dan/brew/opt/php@7.2/bin:$PATH"
#export PATH="/Users/dan/brew/opt/php@7.2/bin:$PATH"

#########################################
#       Homebrew opts                   #
#########################################
export HOMEBREW_CASK_OPTS="--appdir=$HOME/Applications"
