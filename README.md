# Dotfiles

My personal dotfiles for OSX.

## Installation

```
git clone git@bitbucket.org:dpniel_ch/dotfiles.git ~/.dotfiles
ln -s ~/.dotfiles/.zshrc ~/.zshrc
ln -s ~/.dotfiles/.gitignore ~/.gitignore
ln -s ~/.dotfiles/.nanorc ~/.nanorc
```
